﻿angular.module("app")
    .controller("errorMsgCtrl", ["$scope", "$rootScope", function($scope, $rootScope) {
        try {
            $rootScope.Loaded = true;
        } catch (e) {
            console.log(e);
        }
    }]);